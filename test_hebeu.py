import pytest
def setup_function():
 print('---开始计算---')
def teardown_function():
 print('---结束计算---')
def teardown():
 print("---结束测试---")

def add(x,y):
    return x + y
@pytest.mark.hebeu
def test_demo1():
     assert add(3,1) == 4
@pytest.mark.hebeu
def test_demo2():
     assert add(2,2) == 4
@pytest.mark.hebeu
def test_demo3():
     assert add(-5,3) == -2
@pytest.mark.hebeu
def test_demo4():
     assert add(5.23,32.21) == 37.44
@pytest.mark.hebeu
def test_demo5():
    assert add(-88, 88) == 0